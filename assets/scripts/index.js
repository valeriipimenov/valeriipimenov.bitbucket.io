$(window).on('load',function(){
    const body = document.getElementById('body');
    const domElemMap = document.getElementById('map');
    const navBurger = document.querySelector('.nav-burger');
    const menu = document.querySelector('.menu');
    const btnReserve = document.querySelectorAll('.btn-reserve');
    
    const initMap = () => {
        const map = new google.maps.Map(domElemMap, {
            center: {lat: 59.9388, lng: 30.32308},
            zoom: 17
        });
        new google.maps.Marker({
            position: {lat: 59.9388, lng: 30.32308},
            map: map,
            icon: 'assets/image/icon/cat-map-icon.svg'
        });
    };
    
    const toggleViewMenu = () => {
        navBurger.classList.toggle('nav-burger--active');
        body.classList.toggle('body--burger-active');
        menu.classList.toggle('nav-burger--active');
    };
    
    const btnReserveAnimation = () => {
        for( let i = 0; i < btnReserve.length; i++){
            btnReserve[i].addEventListener('click', () => {
                const title = btnReserve[i].children[0];
                const icon = btnReserve[i].children[1];
                
                if( !title.classList.contains('btn-reserve__title--active')){
                    title.textContent = 'Забронировано';
                } else {
                    title.textContent = 'Забронировать'; 
                }
                title.classList.toggle('btn-reserve__title--active');
                icon.classList.toggle('btn-reserve_icon--active');
            });
        };
    };
    
    $('a[href*="#"]').on('click',function() { 
        if(body.classList.contains('body-burger-active')){
            toggleViewMenu();
        }
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1500);
        return false;
    });
    
    $('.rooms__slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        appendArrows: $('.rooms-arrows'),
        appendDots: $('.rooms-dots'),
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    arrows: false
                }
            },
            {
                breakpoint: 606,
                settings: {
                    arrows: false
                }
            }
        ]
    });
    
    $('.reviews__slider').slick({
        dots: true,
        infinite: true,
        variableWidth: true,
        slidesToShow: 1,
        appendArrows: $('.reviews-arrows'),
        appendDots: $('.reviews-dots'),
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    arrows: false
                }
            },
            {
                breakpoint: 606,
                settings: {
                    arrows: false,
                    // variableWidth: false,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: false,
                    variableWidth: false,
                }
            }
        ]
    });

    $('.nav-burger').on('click', () => {
        toggleViewMenu();
    });
    
    initMap();
    btnReserveAnimation();
});









